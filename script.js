var lolChart = {
  donut: function(inp) {
    let a = inp.data||[15, 30];
    const b = inp.element;
    const c = inp.colors||[];
    const d = inp.width||200;
    let e = inp.labels||['Data 1', 'Data 2'];
    let j = true;
    inp.styles = inp.styles||{};
    const p = {hole: inp.styles.hole||{},legends:inp.styles.legends||{}};
    let tt = a.reduce((p, l) => { return p + l; });
    const m = document.createElement('div');
    m.style.width = `${d}px`;
    m.style.height = `${d}px`;
    const par = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    par.setAttribute('width', `100%`);
    par.setAttribute('height', `100%`);
    par.setAttribute('viewBox', '-10 -10 60 60');
    var donut = {
      a: document.createElementNS('http://www.w3.org/2000/svg', 'circle'),
      b: document.createElementNS('http://www.w3.org/2000/svg', 'circle')
    };
    par.style.transition = 'opacity .3s';
    par.oncontextmenu = (p => { p.preventDefault(); });
    donut.a.setAttribute('cx', 21);
    donut.b.setAttribute('cx', 21);
    donut.a.setAttribute('cy', 21);
    donut.b.setAttribute('cy', 21);
    donut.a.setAttribute('r', 15.91549430918954);
    donut.b.setAttribute('r', 15.91549430918954);
    donut.a.setAttribute('fill', p.hole.fill||'#fff');
    donut.b.setAttribute('fill', 'transparent');
    donut.b.setAttribute('stroke', '#d2d3d4');
    donut.b.setAttribute('stroke-width', 12);
    let arr = [];
    let currentoffs = 25;
    par.appendChild(donut.b);
    const tltip = document.createElement('div');
    tltip.style.opacity = 0;
    tltip.style.borderRadius = '8px';
    tltip.style.backgroundColor = '#474747';
    tltip.style.color = '#fff';
    tltip.style.display = 'none';
    tltip.style.position = 'absolute';
    tltip.style.width = 'max-content';
    tltip.style.padding = '8px';
    tltip.style.transition = 'opacity .4s, top .25s, left .25s';
    tltip.style.pointerEvents = 'none';
    donut.a.addEventListener('mousemove', () => {
      tltip.innerHTML = '';
    });
    donut.b.addEventListener('mousemove', () => {
      tltip.innerHTML = '';
    });
    let n;
    par.addEventListener('mousemove', (m) => {
      tltip.style.top = m.clientY+'px';
      tltip.style.left = m.clientX+'px';
    });
    const labs = document.createElement('div');
    labs.style.display = 'flex';
    labs.style.flexWrap = 'wrap';
    labs.style.width = d;
    labs.style.userSelect = 'none';
    labs.style.webkitUserSelect = 'none';
    labs.style.color = p.legends.color||'#000';
    labs.style.backgroundColor = p.legends.bgColor||'transparent';
    labs.style.borderRadius = 
    function lo() {
      a.forEach((v, i) => {
        const q = '#' + ((1<<24)*Math.random() | 0).toString(16);
        const y = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        y.setAttribute('cx', 21);
        y.setAttribute('cy', 21);
        y.setAttribute('r', 15.91549430918954);
        y.style.transition = 'stroke-width .3s';
        y.style.strokeWidth = 19;
        y.setAttribute('stroke-dasharray', `${v / tt * 100} ${100 - (v / tt * 100)}`);
        y.setAttribute('stroke-dashoffset', currentoffs);
        y.setAttribute('stroke', c[i]||q);
        y.setAttribute('fill', 'transparent');
        y.addEventListener('mouseenter', (m) => {
          tltip.style.top = m.clientY+'px';
          tltip.style.left = m.clientX+'px';
          tltip.innerHTML = `<span style="vertical-align: middle; display: inline-block; width: 1em; height: 1em; border-radius: 8px; background-color: ${c[i]||q}"></span> ${e[i]||'-'}: ${v} (${(v / tt * 100).toFixed(1)}%)`;
          if(j) {
            tltip.style.display = 'block';
            setTimeout(() => {
              tltip.style.opacity = '.85';
            }, 90);
          }
        });
        y.addEventListener('mouseleave', m => {
          tltip.style.display = 'none';
          tltip.style.opacity = '0';
        });
        const r = document.createElement('span');
        r.addEventListener('mouseenter', () => {
          y.style.strokeWidth = 22;
        });
        r.addEventListener('click', () => {
          y.style.strokeWidth = 22;
        });
        r.addEventListener('mouseleave', () => {
          y.style.strokeWidth = 19;
        });
        r.style.margin = '12px';
        r.title = `${e[i]||'-'}: ${Math.round(v / tt * 100)}%`;
        r.innerHTML = `<span style="vertical-align: middle; display: inline-block; width: 1em; height: 1em; border-radius: 8px; background-color: ${c[i]||q}"></span> ${e[i]||'-'}`;
        labs.appendChild(r);
        par.appendChild(y);
        arr.push(v / tt * 100);
        currentoffs = 100 - arr.reduce((k, m) => { return k + m; }) + 25;
      });
    }
    lo();
    par.appendChild(donut.a);
    m.appendChild(par);
    m.appendChild(labs);
    m.appendChild(tltip);
    b.appendChild(m);
    var fncs = {
      toggleTooltip: w => {
        if(typeof w !== 'boolean') return;
        j = w;
        return fncs;
      },
      toggleLegends: w => {
        if(typeof w !== 'boolean') return;
        if(w) { labs.style.display = 'flex'; } else { labs.style.display = 'none'; };
        return fncs;
      }, 
      updateData: nw => {
        par.style.opacity = 0;
        labs.style.opacity = 0;
        setTimeout(() => {
          par.innerHTML = '';
          par.appendChild(donut.b);
          labs.innerHTML = '';
          a = nw;
          tt = a.reduce((p, l) => { return p + l; });
          lo();
          par.appendChild(donut.a);
          par.style.opacity = 1;
          labs.style.opacity = 1;
        }, 301);
        return fncs;
      },
      updateLabels: nw => {
        par.style.opacity = 0;
        labs.style.opacity = 0;
        setTimeout(() => {
          par.innerHTML = '';
          par.appendChild(donut.b);
          labs.innerHTML = '';
          e = nw;
          tt = a.reduce((p, l) => { return p + l; });
          lo();
          par.appendChild(donut.a);
          par.style.opacity = 1;
          labs.style.opacity = 1;
        }, 301);
        return fncs;
      }
    };
    return fncs;
  }
};
